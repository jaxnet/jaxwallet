// Copyright (c) 2013-2016 The btcsuite developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package chain

import (
	"context"
	"errors"
	"sync"
	"time"

	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/gcs"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/gcs/builder"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
	"gitlab.com/jaxnet/jaxwallet/netparams"
	"gitlab.com/jaxnet/jaxwallet/waddrmgr"
	"gitlab.com/jaxnet/jaxwallet/wtxmgr"
)

type msgBus struct {
	enqueueNotification chan interface{}
	dequeueNotification chan interface{}
	currentBlock        chan *waddrmgr.BlockStamp
}

// RPCClient represents a persistent client connection to a bitcoin RPC server
// for information regarding the current best block chain.
type RPCClient struct {
	chainID uint32
	rpc     *rpcclient.Client
	// rpc               map[uint32]*rpcclient.Client
	connConfig        *rpcclient.ConnConfig // Work around unexported field
	chainParams       *netparams.Params
	reconnectAttempts int

	// msgBus map[uint32]*msgBus
	msgBus *msgBus

	quitCtx context.Context
	cancel  context.CancelFunc

	wg      sync.WaitGroup
	started bool
	quitMtx sync.Mutex
	// birthday is the earliest time for which we should begin scanning the
	// chain.
	birthday time.Time
}

// NewRPCClient creates a client connection to the server described by the
// connect string.  If disableTLS is false, the remote RPC certificate must be
// provided in the certs slice.  The connection is not established immediately,
// but must be done using the Start method.  If the remote server does not
// operate on the same bitcoin network as described by the passed chain
// parameters, the connection will be disconnected.
func NewRPCClient(chainParams *netparams.Params, chainID uint32, connect, user, pass string,
	certs []byte, disableTLS bool, reconnectAttempts int) (*RPCClient, error) {
	if reconnectAttempts < 0 {
		return nil, errors.New("reconnectAttempts must be positive")
	}

	quitCtx, cancel := context.WithCancel(context.Background())
	client := &RPCClient{
		connConfig: &rpcclient.ConnConfig{
			Params:               chainParams.Beacon.Name,
			Host:                 connect,
			Endpoint:             "ws",
			User:                 user,
			Pass:                 pass,
			Certificates:         certs,
			DisableAutoReconnect: false,
			DisableConnectOnNew:  true,
			DisableTLS:           true,
			// DisableTLS:           disableTLS,
		},
		// rpc:               map[uint32]*rpcclient.Client{},
		chainParams:       chainParams,
		reconnectAttempts: reconnectAttempts,
		// msgBus:            map[uint32]*msgBus{},
		quitCtx: quitCtx,
		cancel:  cancel,
	}

	ntfnCallbacks := &rpcclient.NotificationHandlers{
		OnClientConnected:   client.onClientConnect,
		OnBlockConnected:    client.onBlockConnected,
		OnBlockDisconnected: client.onBlockDisconnected,
		OnRecvTx:            client.onRecvTx,
		OnRedeemingTx:       client.onRedeemingTx,
		OnRescanFinished:    client.onRescanFinished,
		OnRescanProgress:    client.onRescanProgress,
	}

	// for chainID := range chainParams.Chains() {
	// 	client.msgBus[chainID] = &msgBus{
	client.msgBus = &msgBus{
		enqueueNotification: make(chan interface{}),
		dequeueNotification: make(chan interface{}),
		currentBlock:        make(chan *waddrmgr.BlockStamp),
	}
	client.connConfig.ShardID = chainID
	rpcClient, err := rpcclient.New(client.connConfig, ntfnCallbacks)
	if err != nil {
		return nil, err
	}
	client.rpc = rpcClient
	// client.rpc[chainID] = rpcClient
	// }

	return client, nil
}

func (c *RPCClient) RPC(chainID uint32) *rpcclient.Client {
	// return c.rpc[chainID]
	return c.rpc.ForShard(chainID)
}

// BackEnd returns the name of the driver.
func (c *RPCClient) BackEnd() string {
	return "jaxnetd"
}

// Start attempts to establish a client connection with the remote server.
// If successful, handler goroutines are started to process notifications
// sent by the server.  After a limited number of connection attempts, this
// function gives up, and therefore will not block forever waiting for the
// connection to be established to a server that may not exist.
func (c *RPCClient) Start() error {
	// for chainID := range c.chainParams.Chains() {
	// err := c.rpc[chainID].Connect(c.reconnectAttempts)
	err := c.rpc.Connect(c.reconnectAttempts)
	if err != nil {
		return err
	}

	// Verify that the server is running on the expected network.
	// net, err := c.rpc[chainID].GetCurrentNet()
	net, err := c.rpc.GetCurrentNet()
	if err != nil {
		// c.rpc[chainID].Disconnect()
		c.rpc.Disconnect()
		return err
	}
	if net != c.chainParams.Beacon.Net {
		c.rpc.Disconnect()
		// c.rpc[chainID].Disconnect()
		return errors.New("mismatched networks")
	}
	// }
	c.quitMtx.Lock()
	c.started = true
	c.quitMtx.Unlock()

	for chainID := range c.chainParams.Chains() {
		c.wg.Add(1)
		go c.handler(chainID)
	}

	return nil
}

// Stop disconnects the client and signals the shutdown of all goroutines
// started by Start.
func (c *RPCClient) Stop() {
	c.quitMtx.Lock()
	select {
	case <-c.quitCtx.Done():
	default:
		c.cancel()
		// for chainID := range c.chainParams.Chains() {
		// 	c.rpc[chainID].Shutdown()
		c.rpc.Shutdown()
		// }

		if !c.started {
			// for chainID := range c.chainParams.Chains() {
			close(c.msgBus.dequeueNotification)
			// close(c.msgBus[chainID].dequeueNotification)
			// }

		}
	}
	c.quitMtx.Unlock()
}

func (c *RPCClient) GetBestBlock(chainID uint32) (*chainhash.Hash, int32, error) {
	// return c.rpc[chainID].ForShard(chainID).GetBestBlock()
	return c.rpc.ForShard(chainID).GetBestBlock()
}

func (c *RPCClient) GetBlockHash(chainID uint32, height int64) (*chainhash.Hash, error) {
	// return c.rpc[chainID].ForShard(chainID).GetBlockHash(height)
	return c.rpc.ForShard(chainID).GetBlockHash(height)
}

func (c *RPCClient) NotifyReceived(chainID uint32, filter []jaxutil.Address) error {
	// return c.rpc[chainID].ForShard(chainID).NotifyReceived(filter)
	return c.rpc.ForShard(chainID).NotifyReceived(filter)
}

func (c *RPCClient) NotifyBlocks(chainID uint32) error {
	// return c.rpc[chainID].ForShard(chainID).NotifyBlocks()
	return c.rpc.ForShard(chainID).NotifyBlocks()
}

// IsCurrent returns whether the chain backend considers its view of the network
// as "current".
func (c *RPCClient) IsCurrent(chainID uint32) bool {
	// bestHash, _, err := c.rpc[chainID].ForShard(chainID).GetBestBlock()
	bestHash, _, err := c.rpc.ForShard(chainID).GetBestBlock()
	if err != nil {
		return false
	}

	bestHeader, err := c.GetBlockHeader(chainID, bestHash)
	if err != nil {
		return false
	}
	return bestHeader.Timestamp().After(time.Now().Add(-isCurrentDelta))
}

// SetBirthday sets the birthday of the bitcoind rescan client.
//
// NOTE: This should be done before the client has been started in order for it
// to properly carry its duties.
func (c *RPCClient) SetBirthday(t time.Time) {
	c.birthday = t
}

func (c *RPCClient) GetBlock(chainID uint32, hash *chainhash.Hash) (*wire.MsgBlock, error) {
	var err error
	var blockResult *rpcclient.BlockResult

	if chainID == 0 {
		// blockResult, err = c.rpc[chainID].ForBeacon().GetBeaconBlock(hash)
		blockResult, err = c.rpc.ForBeacon().GetBeaconBlock(hash)
	} else {
		// blockResult, err = c.rpc[chainID].ForShard(chainID).GetShardBlock(hash)
		blockResult, err = c.rpc.ForShard(chainID).GetShardBlock(hash)
	}

	if err != nil {
		return nil, err
	}

	return blockResult.Block, nil
}

func (c *RPCClient) GetBlockHeader(chainID uint32, hash *chainhash.Hash) (wire.BlockHeader, error) {
	var err error
	var blockHeader wire.BlockHeader

	if chainID == 0 {
		// blockHeader, err = c.rpc[chainID].ForBeacon().GetBeaconBlockHeader(hash)
		blockHeader, err = c.rpc.ForBeacon().GetBeaconBlockHeader(hash)
	} else {
		// blockHeader, err = c.rpc[chainID].ForShard(chainID).GetShardBlockHeader(hash)
		blockHeader, err = c.rpc.ForShard(chainID).GetShardBlockHeader(hash)
	}

	if err != nil {
		return nil, err
	}

	return blockHeader, nil
}

func (c *RPCClient) SendRawTransaction(chainID uint32, tx *wire.MsgTx, b bool) (*chainhash.Hash, error) {
	// return c.rpc[chainID].ForShard(chainID).SendRawTransaction(tx)
	return c.rpc.ForShard(chainID).SendRawTransaction(tx)
}

// Rescan wraps the normal Rescan command with an additional parameter that
// allows us to map an outpoint to the address in the chain that it pays to.
// This is useful when using BIP 158 filters as they include the prev pkScript
// rather than the full outpoint.
func (c *RPCClient) Rescan(chainID uint32, startHash *chainhash.Hash, addrs []jaxutil.Address,
	outPoints map[wire.OutPoint]jaxutil.Address) error {

	flatOutpoints := make([]*wire.OutPoint, 0, len(outPoints))
	for ops := range outPoints {
		ops := ops

		flatOutpoints = append(flatOutpoints, &ops)
	}

	// return c.rpc[chainID].ForShard(chainID).Rescan(startHash, addrs, flatOutpoints) // nolint:staticcheck
	return c.rpc.ForShard(chainID).Rescan(startHash, addrs, flatOutpoints) // nolint:staticcheck
}

// WaitForShutdown blocks until both the client has finished disconnecting
// and all handlers have exited.
func (c *RPCClient) WaitForShutdown() {
	// for chainID := range c.chainParams.Chains() {
	// 	c.rpc[chainID].WaitForShutdown()
	c.rpc.WaitForShutdown()
	// }
	c.wg.Wait()
}

// Notifications returns a channel of parsed notifications sent by the remote
// bitcoin RPC server.  This channel must be continually read or the process
// may abort for running out memory, as unread notifications are queued for
// later reads.
func (c *RPCClient) Notifications(chainID uint32) <-chan interface{} {
	// return c.msgBus[chainID].dequeueNotification
	return c.msgBus.dequeueNotification
}

// BlockStamp returns the latest block notified by the client, or an error
// if the client has been shut down.
func (c *RPCClient) BlockStamp(chaiID uint32) (*waddrmgr.BlockStamp, error) {
	select {
	// case bs := <-c.msgBus[chaiID].currentBlock:
	case bs := <-c.msgBus.currentBlock:
		return bs, nil
	case <-c.quitCtx.Done():
		return nil, errors.New("disconnected")
	}
}

// FilterBlocks scans the blocks contained in the FilterBlocksRequest for any
// addresses of interest. For each requested block, the corresponding compact
// filter will first be checked for matches, skipping those that do not report
// anything. If the filter returns a positive match, the full block will be
// fetched and filtered. This method returns a FilterBlocksResponse for the first
// block containing a matching address. If no matches are found in the range of
// blocks requested, the returned response will be nil.
func (c *RPCClient) FilterBlocks(chainID uint32, req *FilterBlocksRequest) (*FilterBlocksResponse, error) {

	blockFilterer := NewBlockFilterer(c.chainParams.Beacon, req)

	// Construct the watchlist using the addresses and outpoints contained
	// in the filter blocks request.
	watchList, err := buildFilterBlocksWatchList(req)
	if err != nil {
		return nil, err
	}

	// Iterate over the requested blocks, fetching the compact filter for
	// each one, and matching it against the watchlist generated above. If
	// the filter returns a positive match, the full block is then requested
	// and scanned for addresses using the block filterer.
	for i, blk := range req.Blocks {
		// rawFilter, err := c.rpc[chainID].ForShard(chainID).GetCFilter(&blk.Hash, wire.GCSFilterRegular)
		rawFilter, err := c.rpc.ForShard(chainID).GetCFilter(&blk.Hash, wire.GCSFilterRegular)
		if err != nil {
			return nil, err
		}

		// Ensure the filter is large enough to be deserialized.
		if len(rawFilter.Data) < 4 {
			continue
		}

		filter, err := gcs.FromNBytes(
			builder.DefaultP, builder.DefaultM, rawFilter.Data,
		)
		if err != nil {
			return nil, err
		}

		// Skip any empty filters.
		if filter.N() == 0 {
			continue
		}

		key := builder.DeriveKey(&blk.Hash)
		matched, err := filter.MatchAny(key, watchList)
		if err != nil {
			return nil, err
		} else if !matched {
			continue
		}

		log.Infof("Fetching block height=%d hash=%v",
			blk.Height, blk.Hash)

		rawBlock, err := c.GetBlock(chainID, &blk.Hash)
		if err != nil {
			return nil, err
		}

		if !blockFilterer.FilterBlock(rawBlock) {
			continue
		}

		// If any external or internal addresses were detected in this
		// block, we return them to the caller so that the rescan
		// windows can widened with subsequent addresses. The
		// `BatchIndex` is returned so that the caller can compute the
		// *next* block from which to begin again.
		resp := &FilterBlocksResponse{
			BatchIndex:         uint32(i),
			BlockMeta:          blk,
			FoundExternalAddrs: blockFilterer.FoundExternal,
			FoundInternalAddrs: blockFilterer.FoundInternal,
			FoundOutPoints:     blockFilterer.FoundOutPoints,
			RelevantTxns:       blockFilterer.RelevantTxns,
		}

		return resp, nil
	}

	// No addresses were found for this range.
	return nil, nil
}

// buildFilterBlocksWatchList constructs a watchlist used for matching against a
// cfilter from a FilterBlocksRequest. The watchlist will be populated with all
// external addresses, internal addresses, and outpoints contained in the
// request.
func buildFilterBlocksWatchList(req *FilterBlocksRequest) ([][]byte, error) {
	// Construct a watch list containing the script addresses of all
	// internal and external addresses that were requested, in addition to
	// the set of outpoints currently being watched.
	watchListSize := len(req.ExternalAddrs) +
		len(req.InternalAddrs) +
		len(req.WatchedOutPoints)

	watchList := make([][]byte, 0, watchListSize)

	for _, addr := range req.ExternalAddrs {
		p2shAddr, err := txscript.PayToAddrScript(addr)
		if err != nil {
			return nil, err
		}

		watchList = append(watchList, p2shAddr)
	}

	for _, addr := range req.InternalAddrs {
		p2shAddr, err := txscript.PayToAddrScript(addr)
		if err != nil {
			return nil, err
		}

		watchList = append(watchList, p2shAddr)
	}

	for _, addr := range req.WatchedOutPoints {
		addr, err := txscript.PayToAddrScript(addr)
		if err != nil {
			return nil, err
		}

		watchList = append(watchList, addr)
	}

	return watchList, nil
}

// parseBlock parses a btcws definition of the block a tx is mined it to the
// Block structure of the wtxmgr package, and the block index.  This is done
// here since rpcclient doesn't parse this nicely for us.
func parseBlock(chainID uint32, block *jaxjson.BlockDetails) (*wtxmgr.BlockMeta, error) {
	if block == nil {
		return nil, nil
	}
	blkHash, err := chainhash.NewHashFromStr(block.Hash)
	if err != nil {
		return nil, err
	}
	blk := &wtxmgr.BlockMeta{
		ChainID: chainID,
		Block: wtxmgr.Block{
			Height: block.Height,
			Hash:   *blkHash,
		},
		Time: time.Unix(block.Time, 0),
	}
	return blk, nil
}

func (c *RPCClient) onClientConnect() {
	// for chainID := range c.chainParams.Chains() {
	select {
	// case c.msgBus[chainID].enqueueNotification <- ClientConnected{}:
	case c.msgBus.enqueueNotification <- ClientConnected{}:
	case <-c.quitCtx.Done():
	}
	// }
}

func (c *RPCClient) onBlockConnected(chainID uint32, hash *chainhash.Hash, height int32, time time.Time) {
	select {
	// case c.msgBus[chainID].enqueueNotification <- BlockConnected{
	case c.msgBus.enqueueNotification <- BlockConnected{
		ChainID: chainID,
		Block: wtxmgr.Block{
			Hash:   *hash,
			Height: height,
		},
		Time: time,
	}:
	case <-c.quitCtx.Done():
	}
}

func (c *RPCClient) onBlockDisconnected(chainID uint32, hash *chainhash.Hash, height int32, time time.Time) {
	select {
	// case c.msgBus[chainID].enqueueNotification <- BlockDisconnected{
	case c.msgBus.enqueueNotification <- BlockDisconnected{
		ChainID: chainID,
		Block: wtxmgr.Block{
			Hash:   *hash,
			Height: height,
		},
		Time: time,
	}:
	case <-c.quitCtx.Done():
	}
}

func (c *RPCClient) onRecvTx(chainID uint32, tx *jaxutil.Tx, block *jaxjson.BlockDetails) {
	blk, err := parseBlock(chainID, block)
	if err != nil {
		// Log and drop improper notification.
		log.Errorf("recvtx notification bad block: %v", err)
		return
	}

	rec, err := wtxmgr.NewTxRecordFromMsgTx(tx.MsgTx(), time.Now())
	if err != nil {
		log.Errorf("Cannot create transaction record for relevant "+
			"tx: %v", err)
		return
	}
	select {
	// case c.msgBus[chainID].enqueueNotification <- RelevantTx{chainID, rec, blk}:
	case c.msgBus.enqueueNotification <- RelevantTx{chainID, rec, blk}:
	case <-c.quitCtx.Done():
	}
}

func (c *RPCClient) onRedeemingTx(chainID uint32, tx *jaxutil.Tx, block *jaxjson.BlockDetails) {
	// Handled exactly like recvtx notifications.
	c.onRecvTx(chainID, tx, block)
}

func (c *RPCClient) onRescanProgress(chainID uint32, hash *chainhash.Hash, height int32, blkTime time.Time) {
	select {
	// case c.msgBus[chainID].enqueueNotification <- &RescanProgress{chainID, hash, height, blkTime}:
	case c.msgBus.enqueueNotification <- &RescanProgress{chainID, hash, height, blkTime}:
	case <-c.quitCtx.Done():
	}
}

func (c *RPCClient) onRescanFinished(chainID uint32, hash *chainhash.Hash, height int32, blkTime time.Time) {
	select {
	// case c.msgBus[chainID].enqueueNotification <- &RescanFinished{chainID, hash, height, blkTime}:
	case c.msgBus.enqueueNotification <- &RescanFinished{chainID, hash, height, blkTime}:
	case <-c.quitCtx.Done():
	}

}

// handler maintains a queue of notifications and the current state (best
// block) of the chain.
func (c *RPCClient) handler(chainID uint32) {
	// hash, height, err := c.rpc[chainID].ForShard(chainID).GetBestBlock()
	hash, height, err := c.rpc.ForShard(chainID).GetBestBlock()
	if err != nil {
		log.Errorf("Failed to receive best block from chain server: %v", err)
		c.Stop()
		c.wg.Done()
		return
	}

	bs := &waddrmgr.BlockStamp{Hash: *hash, Height: height}

	// TODO: Rather than leaving this as an unbounded queue for all types of
	// notifications, try dropping ones where a later enqueued notification
	// can fully invalidate one waiting to be processed.  For example,
	// blockconnected notifications for greater block heights can remove the
	// need to process earlier blockconnected notifications still waiting
	// here.

	var notifications []interface{}
	// enqueue := c.msgBus[chainID].enqueueNotification
	enqueue := c.msgBus.enqueueNotification
	var dequeue chan interface{}
	var next interface{}
out:
	for {
		select {
		case n, ok := <-enqueue:
			if !ok {
				// If no notifications are queued for handling,
				// the queue is finished.
				if len(notifications) == 0 {
					break out
				}
				// nil channel so no more reads can occur.
				enqueue = nil
				continue
			}
			if len(notifications) == 0 {
				next = n
				// dequeue = c.msgBus[chainID].dequeueNotification
				dequeue = c.msgBus.dequeueNotification
			}
			notifications = append(notifications, n)

		case dequeue <- next:
			if n, ok := next.(BlockConnected); ok {
				bs = &waddrmgr.BlockStamp{
					Height: n.Height,
					Hash:   n.Hash,
				}
			}

			notifications[0] = nil
			notifications = notifications[1:]
			if len(notifications) != 0 {
				next = notifications[0]
			} else {
				// If no more notifications can be enqueued, the
				// queue is finished.
				if enqueue == nil {
					break out
				}
				dequeue = nil
			}

		// case c.msgBus[chainID].currentBlock <- bs:
		case c.msgBus.currentBlock <- bs:

		case <-c.quitCtx.Done():
			break out
		}
	}

	c.Stop()
	// close(c.msgBus[chainID].dequeueNotification)
	// close(c.msgBus.dequeueNotification)
	c.wg.Done()
}

// POSTClient creates the equivalent HTTP POST rpcclient.Client.
func (c *RPCClient) POSTClient() (*rpcclient.Client, error) {
	configCopy := *c.connConfig
	configCopy.HTTPPostMode = true
	return rpcclient.New(&configCopy, nil)
}
