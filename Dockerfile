# Compile stage
FROM golang:alpine AS build-env
RUN apk add --no-cache git bash

#ENV GOPROXY=direct
ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.com


WORKDIR /jaxnet
ADD . .
RUN go build .

# Final stage
FROM alpine:3.7

# Allow delve to run on Alpine based containers.
RUN apk add --no-cache ca-certificates bash

WORKDIR /

COPY --from=build-env /jaxnet/jaxwallet /
COPY /sample-jaxwallet.conf /

# default rpc port
#EXPOSE 8332

# Run app
CMD ./jaxwallet
