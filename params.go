// Copyright (c) 2013-2015 The btcsuite developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package main

import (
	"gitlab.com/jaxnet/jaxwallet/netparams"
)

var activeNet = &netparams.MainNetParams

func init() {
	activeNet = activeNet.WithEnabledChains(nil)
}
