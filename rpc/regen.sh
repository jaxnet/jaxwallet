#!/bin/sh

#protoc -I. api.proto --go_out=plugins=grpc:walletrpc
#

protoc --go_out=. --go_opt=paths=source_relative \
    --go-grpc_out=. --go-grpc_opt=paths=source_relative \
    api.proto
