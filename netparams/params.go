// Copyright (c) 2013-2015 The btcsuite developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

package netparams

import (
	"github.com/pkg/errors"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

// Params is used to group parameters for various networks such as the main
// network and test networks.
type Params struct {
	Beacon *chaincfg.Params
	Shards map[uint32]*chaincfg.Params

	RPCClientPort string
	RPCServerPort string
	chainIndex    map[uint32]struct{}
}

// MainNetParams contains parameters specific running jaxwallet and
// btcd on the main network (wire.MainNet).
var MainNetParams = Params{
	Beacon:        &chaincfg.MainNetParams,
	RPCClientPort: "8334",
	RPCServerPort: "8332",
}

// TestNet3Params contains parameters specific running jaxwallet and
// btcd on the test network (version 3) (wire.TestNet3).
var TestNet3Params = Params{
	Beacon:        &chaincfg.TestNetParams,
	RPCClientPort: "18334",
	RPCServerPort: "18332",
}

// FastNetParams contains parameters specific running jaxwallet and
// jaxnetd on the test network (wire.FastNet).
var FastNetParams = Params{
	Beacon:        &chaincfg.FastNetParams,
	RPCClientPort: chaincfg.FastNetParams.DefaultRPCPort,
	RPCServerPort: "19335",
}

// SimNetParams contains parameters specific to the simulation test network
// (wire.SimNet).
var SimNetParams = Params{
	Beacon:        &chaincfg.SimNetParams,
	RPCClientPort: "18556",
	RPCServerPort: "18554",
}

func (activeNet *Params) InitShardsParams(client *rpcclient.Client) error {
	shards, err := client.ListShards()
	if err != nil {
		return err
	}
	activeNet.chainIndex = map[uint32]struct{}{0: {}}
	activeNet.Shards = map[uint32]*chaincfg.Params{}
	for id, shardInfo := range shards.Shards {
		blockHash, err := chainhash.NewHashFromStr(shardInfo.BeaconExpansionHash)
		if err != nil {
			return errors.Wrapf(err, "invalid beacon hash(%s) for shard(%d)", shardInfo.BeaconExpansionHash, id)
		}
		block, err := client.ForBeacon().GetBeaconBlock(blockHash)
		if err != nil {
			return errors.Wrapf(err, "can not get beacon block(%s) for shard(%d)", shardInfo.BeaconExpansionHash, id)
		}

		activeNet.Shards[id] = activeNet.Beacon.ShardParams(id, block.Block)
		activeNet.chainIndex[id] = struct{}{}
	}

	return nil
}

type ChainIndex map[uint32]struct{}

func (activeNet *Params) WithEnabledChains(chains []uint32) *Params {
	if len(chains) == 0 {
		activeNet.chainIndex = map[uint32]struct{}{
			0: {},
			1: {},
			2: {},
			3: {},
		}
	}
	for _, u := range chains {
		activeNet.chainIndex[u] = struct{}{}
	}

	return activeNet
}

func (activeNet *Params) Enabled(chainID uint32) bool {
	if activeNet.chainIndex == nil {
		return true
	}

	_, ok := activeNet.chainIndex[chainID]
	return ok
}

func (activeNet *Params) Chain(id uint32) *chaincfg.Params {
	if id == 0 {
		return activeNet.Beacon
	}
	return activeNet.Shards[id]
}

func (activeNet *Params) Chains() map[uint32]struct{} {
	if len(activeNet.chainIndex) == 0 {
		panic("chain params are not configured")
	}
	return activeNet.chainIndex
}
