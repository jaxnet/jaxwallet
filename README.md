jaxwallet
=========

jaxwallet is a daemon handling bitcoin wallet functionality for a single user. It acts as both an RPC client to jaxnetd
and an RPC server for wallet clients and legacy RPC applications.

1. [Getting Started](#Getting-Started)
2. [RPC Usage](#RPC-Usage)


Public and private keys are derived using the hierarchical deterministic format described by
[BIP0032](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki). Unencrypted private keys are not supported
and are never written to disk. jaxwallet uses the
`m/44'/<coin type>'/<account>'/<branch>/<address index>`
HD path for all derived addresses, as described by
[BIP0044](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki).

Due to the sensitive nature of public data in a BIP0032 wallet, jaxwallet provides the option of encrypting not just
private keys, but public data as well. This is intended to thwart privacy risks where a wallet file is compromised
without exposing all current and future addresses (public keys) managed by the wallet. While access to this information
would not allow an attacker to spend or steal coins, it does mean they could track all transactions involving your
addresses and therefore know your exact balance. In a future release, public data encryption will extend to transactions
as well.

jaxwallet is not an SPV client and requires connecting to a local or remote jaxnetd instance for asynchronous blockchain
queries and notifications over websockets. Full jaxnetd installation instructions can be
found [here](https://gitlab.com/jaxnet/jaxnetd). An alternative SPV mode that is compatible with jaxnetd is planned for
a future release.

A legacy JSON-RPC server mostly compatible with Bitcoin Core

The JSON-RPC server exists to ease the migration of wallet applications from Core, but complete compatibility is not
guaranteed. Some portions of the API (and especially accounts) have to work differently due to other design decisions (
mostly due to BIP0044). However, if you find a compatibility issue and feel that it could be reasonably supported,
please report an issue. This server is enabled by default.

## Requirements

[Go](http://golang.org) 1.17 or newer.

## Building, installation and updating

```bash
go version
go env GOROOT GOPATH
```

NOTE: The `GOROOT` and `GOPATH` above must not be the same path. It is recommended that `GOPATH` is set to a directory
in your home directory such as
`~/goprojects` to avoid write permission issues. It is also recommended to add
`$GOPATH/bin` to your `PATH` at this point.

- Run the following commands to obtain jaxwallet, all dependencies, and install it:

```bash
git clone https://gitlab.com/jaxnet/jaxwallet.git
cd jaxwallet
go build -v . 
go build -v . ./cmd/...
```

Now you can use `./jaxwallet` binary.

Alternative way. You can install `jaxwalletd` in ```$GOPATH/bin```.

```bash
go install -v . ./cmd/...
```

- `jaxwallet` (and utilities) will now be installed in ```$GOPATH/bin```. If you did not already add the bin directory
  to your system path during Go installation, we recommend you do so now.

## Updating

#### Linux/BSD/MacOSX/POSIX - Build from Source

- Run the following commands to update jaxnetd, all dependencies, and install it:

```bash
cd jaxwallet
git pull
go build -v . 
go build -v . ./cmd/...
## OR install binaries to $GOPATH/bin
go install -v . ./cmd/...
```

Now you can use `./jaxwallet` binary.

## Getting Started

The following instructions detail how to get started with jaxwallet connecting to a localhost jaxnetd.

1. Setup [jaxnetd](https://gitlab.com/jaxnet/jaxnetd/-/blob/master/README.md) node and/or get `jaxnetd` RPC credentials.
2. Fill the `jaxwallet.conf` configuration file. Here is example:

```ini
[Application Options]

; Set 1 to use testnet otherwise mainnet will be enabled.
testnet = 0

; uncomment and set path to data directory if want to change defualt: $HOME/.jaxwallet
; appdata=./jaxwallet

; uncomment and set, if additional password was configured 
; walletpass=public

; The server and port used for jaxnetd RPC & websocket connections.
rpcconnect = localhost:18333

; File containing root certificates to authenticate a TLS connections with btcd
; cafile=~/.jaxwallet/btcd.cert
; TLS certificate and key file locations
; rpccert=~/.jaxwallet/rpc.cert
; rpckey=~/.jaxwallet/rpc.key
; onetimetlskey=0
noservertls = 1

rpclisten = 127.0.0.1:8337

; This user/password will be used as 
;   user/password for the jaxwallet rpc
;   AND user/password to auth jaxnetd connection.
; If you would like to use different, 
; please setup jaxnetdusername and jaxnetdpassword
username = jax_rpc_user
password = jax_rpc_pass

; jaxnetdusername=
; jaxnetdpassword=
```

3. Initialize wallet state by running this command:

```shell
./jaxwallet -C ./jaxwallet.conf --create
```

4. Follow the instructions.
5. Run the following command to start jaxwallet:

```shell
./jaxwallet -C ./jaxwallet.conf --create
```

## RPC Usage

**jaxwallet** and **jaxnetd**  provides a [JSON-RPC](http://json-rpc.org/wiki/specification) API with *some difference*. There are a few
key differences between **jaxnetd** and **bitcoind**, **btcd** and **JSON-RPC** as far as how RPCs are serviced:

* Unlike **JSON-RPC** the **jaxnetd** has few additional fields:
    1. `scope` - sets the context of the request;
      - `node` methods related to daemon/node, like **getVersion**, **ping**, etc;
      - `chain` common methods same for the `beacon` and `shard`;
      - `beacon` methods that works only for `beacon` chain;
      - `shard`  methods that works only for `shard` chains;
      - `wallet`  methods that works only for `jaxwallet` RPC instance;

    2. `shard_id` - sets to which chain this method will be performed. **0** for `beacon` chain, `>=1` for `shard`
       chains.

Here is example of `getbalance` method for **Shard Chain #1**.
```json
{
  "method": "getbalance",
  "params": [
    "default"
  ],
  "id": 1,
  "scope": "wallet",
  "shard_id": 1
}
``` 

Here is list of enabled method:

```
	// Reference implementation wallet methods (implemented)
	"addmultisigaddress"
	"createmultisig"
	"dumpprivkey"
	"getaccount"
	"getaccountaddress"
	"getaddressesbyaccount"
	"getbalance"
	"getbestblockhash"
	"getblockcount"
	"getinfo"
	"getnewaddress"
	"getrawchangeaddress"
	"getreceivedbyaccount"
	"getreceivedbyaddress"
	"gettransaction"
	"help"
	"importprivkey"
	"keypoolrefill"
	"listaccounts"
	"listlockunspent"
	"listreceivedbyaccount"
	"listreceivedbyaddress"
	"listsinceblock"
	"listtransactions"
	"listunspent"
	"lockunspent"
	"sendfrom"
	"sendmany"
	"sendtoaddress"
	"settxfee"
	"signmessage"
	"signrawtransaction"
	"validateaddress"
	"verifymessage"
	"walletlock"
	"walletpassphrase"
	"walletpassphrasechange"

	// Extensions to the reference client JSON-RPC API
	"createnewaccount"
	"getbestblock"

	// This was an extension but the reference implementation added it as
	// well, but with a different API (no account parameter).  It's listed
	// here because it hasn't been update to use the reference
	// implemenation's API.
	"getunconfirmedbalance"
	"listaddresstransactions"
	"listalltransactions"
	"renameaccount"
	"walletislocked"
```

## Issue Tracker

The [integrated github issue tracker](https://gitlab.com/jaxnet/jaxwallet/issues)
is used for this project.

## GPG Verification Key

All official release tags are signed by Conformal so users can ensure the code has not been tampered with and is coming
from the btcsuite developers. To verify the signature perform the following:

- Download the public key from the Conformal website at
  https://opensource.conformal.com/GIT-GPG-KEY-conformal.txt

- Import the public key into your GPG keyring:
  ```bash
  gpg --import GIT-GPG-KEY-conformal.txt
  ```

- Verify the release tag with the following command where `TAG_NAME` is a placeholder for the specific tag:
  ```bash
  git tag -v TAG_NAME
  ```

## License

jaxwallet is licensed under the liberal ISC License.
