// Copyright (c) 2016 The btcsuite developers
// Use of this source code is governed by an ISC
// license that can be found in the LICENSE file.

// Package txrules provides transaction rules that should be followed by
// transaction authors for wide mempool acceptance and quick mining.
package txrules

import (
	"errors"
	"math"

	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/mempool"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// DefaultRelayFeePerKb is the default minimum relay fee policy for a mempool.
const DefaultRelayFeePerKb jaxutil.Amount = 1e3

// IsDustOutput determines whether a transaction output is considered dust.
// Transactions with dust outputs are not standard and are rejected by mempools
// with default policies.
func IsDustOutput(output *wire.TxOut, relayFeePerKb jaxutil.Amount) bool {
	// Unspendable outputs which solely carry data are not checked for dust.
	if txscript.GetScriptClass(output.PkScript) == txscript.NullDataTy {
		return false
	}

	return mempool.IsDust(output, relayFeePerKb)
}

// Transaction rule violations
var (
	ErrAmountNegative = errors.New("transaction output amount is negative")
	ErrOutputIsDust   = errors.New("transaction output is dust")
	// ErrAmountExceedsMax = errors.New("transaction output amount exceeds maximum value")
)

// CheckOutput performs simple consensus and policy tests on a transaction
// output.
func CheckOutput(output *wire.TxOut, relayFeePerKb jaxutil.Amount) error {
	if output.Value < 0 {
		return ErrAmountNegative
	}
	// if output.Value > jaxutil.MaxSatoshi {
	// 	return ErrAmountExceedsMax
	// }
	if IsDustOutput(output, relayFeePerKb) {
		return ErrOutputIsDust
	}
	return nil
}

// FeeForSerializeSize calculates the required fee for a transaction of some
// arbitrary size given a mempool's relay fee policy.
func FeeForSerializeSize(relayFeePerKb jaxutil.Amount, txSerializeSize int) jaxutil.Amount {
	fee := relayFeePerKb * jaxutil.Amount(txSerializeSize) / 1000

	if fee == 0 && relayFeePerKb > 0 {
		fee = relayFeePerKb
	}
	if fee < 0 {
		fee = math.MaxInt32
	}

	// if fee < 0 || fee > jaxutil.MaxSatoshi {
	// 	fee = jaxutil.MaxSatoshi
	// }

	return fee
}
