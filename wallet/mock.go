package wallet

import (
	"time"

	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
	"gitlab.com/jaxnet/jaxwallet/chain"
	"gitlab.com/jaxnet/jaxwallet/waddrmgr"
)

type mockChainClient struct {
}

var _ chain.Interface = (*mockChainClient)(nil)

func (m *mockChainClient) Start() error {
	return nil
}

func (m *mockChainClient) Stop() {
}

func (m *mockChainClient) WaitForShutdown() {}

func (m *mockChainClient) GetBestBlock(uint32) (*chainhash.Hash, int32, error) {
	return nil, 0, nil
}

func (m *mockChainClient) GetBlock(uint32, *chainhash.Hash) (*wire.MsgBlock, error) {
	return nil, nil
}

func (m *mockChainClient) GetBlockHash(uint32, int64) (*chainhash.Hash, error) {
	return nil, nil
}

func (m *mockChainClient) GetBlockHeader(uint32, *chainhash.Hash) (wire.BlockHeader, error) {
	return nil, nil
}

func (m *mockChainClient) IsCurrent(uint32) bool {
	return false
}

func (m *mockChainClient) FilterBlocks(uint32, *chain.FilterBlocksRequest) (
	*chain.FilterBlocksResponse, error) {
	return nil, nil
}

func (m *mockChainClient) BlockStamp(uint32) (*waddrmgr.BlockStamp, error) {
	return &waddrmgr.BlockStamp{
		Height:    500000,
		Hash:      chainhash.Hash{},
		Timestamp: time.Unix(1234, 0),
	}, nil
}

func (m *mockChainClient) SendRawTransaction(uint32, *wire.MsgTx, bool) (
	*chainhash.Hash, error) {
	return nil, nil
}

func (m *mockChainClient) Rescan(uint32, *chainhash.Hash, []jaxutil.Address,
	map[wire.OutPoint]jaxutil.Address) error {
	return nil
}

func (m *mockChainClient) NotifyReceived(uint32, []jaxutil.Address) error {
	return nil
}

func (m *mockChainClient) NotifyBlocks(uint32) error {
	return nil
}

func (m *mockChainClient) Notifications(uint32) <-chan interface{} {
	return nil
}

func (m *mockChainClient) BackEnd() string {
	return "mock"
}
